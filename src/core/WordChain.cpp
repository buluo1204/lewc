//
// Created by 13281 on 2023/3/13.
//

#include "WordChain.h"

#include <utility>
#include <iostream>

WordChain::WordChain() {
//    words = vector<string>();
    for (int i = 0; i < 26; i++) {
        headMap[i] = false;
        tailMap[i] = false;
    }
}

WordChain::WordChain(const vector<string>& words) {
//    this->words = vector<string>();
    for (int i = 0; i < 26; i++) {
        headMap[i] = false;
        tailMap[i] = false;
    }
    for (const string& word : words) {
        this->append(word);
    }
}

void WordChain::append(Word word) {
    words.push_back(word);
    headMap[word.getHeadLetter() - 'a'] = true;
    tailMap[word.getTailLetter() - 'a'] = true;
    letterNum += word.getLength();
}

void WordChain::append(WordChain wordChain) {
    for (const Word& word : wordChain.getWords()) {
        this->append(word);
    }
}

char WordChain::getHead() {
    if (words.empty()) {
        return '\0';
    }
    return words[0].getHeadLetter();
}

char WordChain::getTail() {
    if (words.empty()) {
        return '\0';
    }
    return words[words.size() - 1].getTailLetter();
}

bool WordChain::contains(char c) {
    if (headMap[::tolower(c)]) {
        return true;
    }
    if (tailMap[::tolower(c)]) {
        return true;
    }
    return false;
}

int WordChain::getWordNum() {
    return words.size();
}

int WordChain::getLetterNum() const {
    return letterNum;
}

string WordChain::toString() {
    string res;
    for (Word word : words) {
        res += word.toString();
        res += " ";
    }
    return res;
}

void WordChain::insertHead(Word word) {
    words.insert(words.begin(), word);
}

WordChain *WordChain::add(Word word) {
    WordChain *wordChain = new WordChain();
    for (const Word& w : words) {
        wordChain->append(w);
    }
    wordChain->append(std::move(word));
    return wordChain;
}

vector<Word> WordChain::getWords() {
    return words;
}
