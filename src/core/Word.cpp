#include <iostream>
#include "Word.h"

using namespace std;

Word::Word(std::string word) {
    this->str = std::move(word);
}

Word::Word(char *word) {
    this->str = string(word);
}

char Word::getHeadLetter() {
    return ::tolower(str[0]);
}

char Word::getTailLetter() {
    return ::tolower(str[this->str.length() - 1]);
}

void Word::setChainsWord(vector<string> c, const int len) {
    this->maxChainsLength = len + 1;
    this->chains.emplace_back(this->str);
    this->chains.insert(this->chains.end(), c.begin(), c.end());
}

void Word::setChainsLetter(std::vector<std::string> c, const int len) {
    this->maxChainsLength = len + this->str.length();
    this->chains.emplace_back(this->str);
    this->chains.insert(this->chains.end(), c.begin(), c.end());
}

int Word::getLength() {
    return (int) str.length();
}

std::string Word::toString() {
    return str;
}

bool Word::operator==(Word &word) {
    return word.str == str;
}
