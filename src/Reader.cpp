//
// Created by 13281 on 2023/3/11.
//

#include <iostream>
#include "Reader.h"

Reader::Reader(const string& filename) {
    ifstream in(filename, ios::in);
    istreambuf_iterator<char> beg(in), end;
    this->content = string(beg, end);
    in.close();
}

string Reader::next() {
    for(head = tail; head < content.length(); head++) {
        if (isalpha(content[head])) {
            break;
        }
    }
    for (tail = head; tail < content.length(); tail++) {
        if (!isalpha(content[tail])) {
            break;
        }
    }
    if (head == content.length()) {
        return "";
    }
    return content.substr(head, tail - head);
}

vector<string> Reader::readAll() {
    auto res = vector<string>();
    while(true) {
        string word = Reader::next();
        if (word.empty()) {
            break;
        }
        res.push_back(word);
    }
    return res;
}
