//
// Created by 13281 on 2023/3/13.
//

#ifndef LEWC_WORDCHAIN_H
#define LEWC_WORDCHAIN_H

#include <string>
#include <vector>

#include "Word.h"

using namespace std;

class WordChain {
private:
    vector<Word> words;
    bool headMap[26];
    bool tailMap[26];
    int letterNum = 0;
public:
    WordChain();

    WordChain(const vector<string>& words);

    void insertHead(Word word);

    void append(Word word);

    void append(WordChain wordChain);

    vector<Word> getWords();

    WordChain *add(Word word);

    char getHead();

    char getTail();

    bool contains(char c);

    int getWordNum();

    int getLetterNum() const;

    string toString();
};


#endif //LEWC_WORDCHAIN_H
