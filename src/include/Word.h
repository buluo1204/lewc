#ifndef LEWC_WORD_H
#define LEWC_WORD_H

#include <string>
#include <vector>

using namespace std;

class Word {
public:
    string str;
    bool genChainVisited = false;
    bool checkCircleVisited = false;
    int maxChainsLength = 0;
    int maxLetterLength = 0;
    vector<std::string> chains;

    Word(string word);
    Word(char *word);
    int getLength();
    char getHeadLetter();
    char getTailLetter();
    void setChainsWord(std::vector<std::string> c, const int len);
    void setChainsLetter(std::vector<std::string> c, const int len);
    bool operator== (Word& word);
    std::string toString();
};


#endif //LEWC_WORD_H
