//
// Created by 13281 on 2023/3/7.
//

#ifndef LEWC_OPTION_H
#define LEWC_OPTION_H

#include <iostream>
#include <string>
#include <cstring>
#include <cassert>

using namespace std;

class Option {
private:
    string filename;
    int n = 0;
    int w = 0;
    int c = 0;
    int h = 0;
    int t = 0;
    int j = 0;
    int r = 0;
    char h_char;
    char t_char;
    char j_char;
    bool error = false;
    bool debug = false;
public:
    Option(int argc, char *argv[]);

    string getFileName();

    int getFunction();

    char getHeadChar();

    char getTailChar();

    char getRejectChar();

    int allowCircles();

    bool isError();

    bool isDebug();
};


#endif //LEWC_OPTION_H
