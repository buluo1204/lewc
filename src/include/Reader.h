//
// Created by 13281 on 2023/3/11.
//

#ifndef LEWC_READER_H
#define LEWC_READER_H

#include <fstream>
#include <string>
#include <vector>

using namespace std;

class Reader {
private:
    string content;
    int head = 0;
    int tail = 0;
public:
    Reader(const string& filename);

    string next();

    vector<string> readAll();
};


#endif //LEWC_READER_H
