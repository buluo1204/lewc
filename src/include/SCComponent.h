//
// Created by 13281 on 2023/3/14.
//

#ifndef LEWC_SCCOMPONENT_H
#define LEWC_SCCOMPONENT_H

#include <set>
#include <vector>
#include <string>
#include <algorithm>

#include "Word.h"
#include "WordChain.h"

using namespace std;

class SCComponent {
private:
    vector<int> nodes;
    vector<Word> edges;
    vector<Word> linkList[26];
    vector<Word> linkMap[26][26];
    Word* longest_link[26][26] = { nullptr };
    vector<WordChain> paths[26][26];
    WordChain *sccLongestPath[26][26] = { nullptr };
    WordChain *sccMostPath[26][26] = { nullptr };
public:
    SCComponent();

    void addNode(int node);

    void addEdge(Word word);

    void searchPaths();

    void searchPathsFrom(int node, WordChain *prevChain, set<string> *passed);

    vector<int> getNodes();

    WordChain getMostPathBetween(int from, int to);

    WordChain getLongestPathBetween(int from, int to);
};


#endif //LEWC_SCCOMPONENT_H
