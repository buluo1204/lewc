//
// Created by 13281 on 2023/3/7.
//

#include "Option.h"

Option::Option(int argc, char **argv) {
//    assert(strcmp(argv[0], "Wordlist.exe") == 0);
    for(int i = 1; i < argc - 1; i++) {
            switch (argv[i][1]) {
                case 'n':
                    this->n = 1;
                    break;
                case 'w':
                    this->w = 1;
                    break;
                case 'c':
                    this->c = 1;
                    break;
                case 'h':
                    this->h = 1;
                    this->h_char = argv[++i][0];
                    break;
                case 't':
                    this->t = 1;
                    this->t_char = argv[++i][0];
                    break;
                case 'j':
                    this->j = 1;
                    this->j_char = argv[++i][0];
                    break;
                case 'r':
                    this->r = 1;
                    break;
                case '-':
                    this->debug = true;
                    break;
                default:
                    break;
            }
        this->filename = string(argv[argc - 1]);
    }
    if (this->n + this->w + this->c != 1) {
        cout << "One of -n, -w, -c should appear once and only once." << endl;
        this->error = true;
    }
//    if (this->j + this->r > 1) {
//        cout << "Argument -j, -r shouldn't appear at the same time." << endl;
//        this->error = true;
//    }
    if (this->n && (this->h || this->t || this->j || this->r)) {
        cout << "Argument -n shouldn't appear with other arguments." << endl;
        this->error = true;
    }
    if (this->h && !isalpha(this->h_char)) {
        cout << "Argument -h must be a-z or A-Z." << endl;
        this->error = true;
    }
    if (this->t && !isalpha(this->t_char)) {
        cout << "Argument -t must be a-z or A-Z." << endl;
        this->error = true;
    }
    if (this->j && !isalpha(this->j_char)) {
        cout << "Argument -j must be a-z or A-Z." << endl;
        this->error = true;
    }
//    if (this->j_char == this->h_char) {
//        cout << "The qualified word chain does not exist." << endl;
//        this->error = true;
//    }
    if (this->filename.empty()) {
        cout << "Filename must not be empty." << endl;
        this->error = true;
    }
}

string Option::getFileName() {
    return this->filename;
}

int Option::getFunction() {
    if (this->n == 1) {
        return 1;
    } else if (this->w == 1) {
        return 2;
    } else if (this->c == 1) {
        return 3;
    }
    return 0;
}

char Option::getHeadChar() {
    if (this->h == 0) {
        return '\0';
    }
    return this->h_char;
}

char Option::getTailChar() {
    if (this->t == 0) {
        return '\0';
    }
    return this->t_char;
}

char Option::getRejectChar() {
    if (this->j == 0) {
        return '\0';
    }
    return this->j_char;
}

int Option::allowCircles() {
    return this->r;
}

bool Option::isError() {
    return this->error;
}

bool Option::isDebug() {
    return this->debug;
}

